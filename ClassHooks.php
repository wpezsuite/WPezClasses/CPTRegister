<?php
/**
 * ClassHooks.php
 *
 * @package WPezSuite\WPezClasses\CPTRegister;
 */

namespace WPezSuite\WPezClasses\CPTRegister;

// No WP? No go.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}
/**
 * Class to register the hooks that register a custom post type.
 */
class ClassHooks {

	/**
	 * An instance of the InterfaceCPTRegister
	 *
	 * @var object
	 */
	protected $new_component;

	/**
	 * Array of the defaults for the hooks.
	 *
	 * @var array
	 */
	protected $arr_hook_defaults;

	/**
	 * Array of the action we're going to register.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * ClassHooks constructor.
	 *
	 * @param InterfaceCPTRegister $obj The InterfaceCPTRegister object.
	 */
	public function __construct( InterfaceCPTRegister $obj ) {

		$this->new_component = $obj;
		$this->setPropertyDefaults();
	}

	/**
	 * Sets the property defaults.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_hook_defaults = array(
			'active'        => true,
			'component'     => $this->new_component,
			'priority'      => '10',
			'accepted_args' => '1',
		);

		$this->arr_actions = array();

		$this->arr_actions['register'] = array(
			'hook'     => 'init',
			'callback' => 'registerPostTypes',
		);
	}


	/**
	 * Updates the $arr_hook_defaults with the $arr.
	 *
	 * @param array $arr Array of updates to the arr_hook_defaults.
	 *
	 * @return array
	 */
	public function updateHookDefaults( array $arr = array() ) {

		$this->arr_hook_defaults = array_merge( $this->arr_hook_defaults, $arr );
		return $this->arr_hook_defaults;
	}

	/**
	 * Get the $arr_actions property.
	 *
	 * @return array
	 */
	public function getActions() {

		return $this->arr_actions;
	}

	/**
	 * Update the $arr_actions property.
	 *
	 * @param array $arr Array of updates to the arr_actions.
	 *
	 * @return bool
	 */
	public function updateActions( array $arr = array() ) {

		$this->arr_actions = array_merge( $this->arr_actions, $arr );
		return $this->arr_actions;
	}

	/**
	 * Register the actions.
	 *
	 * @param array $arr_exclude Array of actions to be excluded from registering.
	 *
	 * @return void
	 */
	public function register( array $arr_exclude = array() ) {

		foreach ( $this->arr_actions as $str_ndx => $arr_action ) {

			if ( in_array( $str_ndx, $arr_exclude, true ) ) {
				continue;
			}

			$arr = array_merge( $this->arr_hook_defaults, $arr_action );
			if ( false === $arr['active'] ) {
				continue;
			}

			add_action(
				$arr['hook'],
				array( $arr['component'], $arr['callback'] ),
				$arr['priority'],
				$arr['accepted_args']
			);
		}
	}
}
