## WPezClasses: Custom Post Type Register

__Register your WordPress custom post types The ezWay.__

Along with CPTLabels and CPTArgs, the circle is now complete.
   

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._


    use WPezSuite\WPezClasses\CPTLabelsUS\ClassCPTLabelsUS as Labels;
    use WPezSuite\WPezClasses\CPTArgs\ClassCPTArgs as CPTArgs;
    use WPezSuite\WPezClasses\CPTRegister\ClassCPTRegister as CPTReg;
    use WPezSuite\WPezClasses\CPTRegister\ClassHooks as Hooks;
    
    $new_labels = new Labels();
    $new_labels->setNames('TEST CPT');
    $arr_labels = $new_labels->getLabels();

    // Note: There is not need to wrap all this in an add_action(), the Hooks class takes care of that.
    // In fact, if you do use an add_action() it's likely you're CPT won't register. 
    
    $new_args = new CPTArgs();
    $new_args->setLabels($arr_labels);
    // Custom args that deviate from the standard defaults.
    $arr_args = array(
	      'public'       => true,
	      'show_in_menu' => true,
	      'show_ui'      => true,
        'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
      );
    $new_args->loadArgs( $arr_args );
    $arr_args = $new_args->getArgsAll();
    
    $new_reg = new CPTReg();
    $new_reg->pushCPT('my_test_cpt',  $arr_args);
    
    $new_hooks = new Hooks($new_reg);
    $new_hooks->register();
    
    



### FAQ

__1) Why?__

Truth be told, you could take the args and do your own register_post_type(). However, The ezWay eliminates having to know that hook, create that function / method, etc. What's normally many lines of code has been reduced to 10 - 15 lines of simplified configuring. 

Less thinking and typing, more doing; such is The ezWay. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- https://gitlab.com/wpezsuite/WPezClasses/CPTLabelsUS

- https://gitlab.com/wpezsuite/WPezClasses/CPTArgs

- https://codex.wordpress.org/Function_Reference/register_post_type



### TODO



### CHANGE LOG

-v0.0.3 - Sun 8 Jan 2023
  - Refactoring + doc comments, and updated README.


- v0.0.2 - Monday 22 April 2019
  - Updated: interface file / name


- v0.0.2 - Monday 15 April 2019
  - Updated: Hooks -> updateActions() 


- v0.0.1 - Monday 15 April 2019
  - This has been in development for quite some time, but not independently repo'ed until now.