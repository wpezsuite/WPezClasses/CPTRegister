<?php
/**
 * InterfaceCPTRegister.php
 *
 * @package WPezSuite\WPezClasses\CPTRegister;
 */

namespace WPezSuite\WPezClasses\CPTRegister;

/**
 * Interface for registering a custom post type.
 */
interface InterfaceCPTRegister {

	/**
	 * Given an array of CTPs and their args, register them.
	 */
	public function registerPostTypes();

}
