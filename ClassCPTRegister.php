<?php
/**
 * WPezClasses - Class CPTRegister
 *
 * @package WPezSuite\WPezClasses\CPTRegister
 */

namespace WPezSuite\WPezClasses\CPTRegister;

// No WP? No go.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * Class to register a custom post type.
 */
class ClassCPTRegister implements InterfaceCPTRegister {

	/**
	 * Array of CTPs to register.
	 *
	 * @var array
	 */
	protected $arr_cpts;

	/**
	 * Array of results of the CTPs registered.
	 *
	 * @var array
	 */
	protected $arr_ret;

	/**
	 * ClassCPTRegister constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Sets the property defaults.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_cpts = array();
		$this->arr_ret  = array();
	}

	/**
	 * Push a single CTP (and its args) to the array of CTPs to register.
	 *
	 * @param string $str_post_type The CTP name.
	 * @param array  $arr_args      The CTP args.
	 *
	 * @return bool
	 */
	public function pushCPT( string $str_post_type = '', array $arr_args = array() ) {

		$str_post_type = trim( $str_post_type );

		if ( strlen( $str_post_type ) < 21 ) {

			$this->arr_cpts[ $str_post_type ] = $arr_args;
			return true;
		}
		return false;
	}

	/**
	 * Push an array of CTPs (and their args) to the array of CTPs to register.
	 *
	 * @param array $arr_cpts - array of CTPs to register. CTP name / slug in the keys.
	 *
	 * @return bool
	 */
	public function loadCPTs( array $arr_cpts = array() ) {

		if ( ! empty( $arr_cpts ) ) {
			$arr_ret = array();
			foreach ( $arr_cpts as $str_pt => $arr_args ) {

				$str_pt             = trim( $str_pt );
				$arr_ret[ $str_pt ] = $this->pushCPT( $str_pt, $arr_cpts );
			}
			return $arr_ret;
		}
		// Something went wrong.
		return false;
	}

	/**
	 * Registers the CTPs.
	 *
	 * @return void
	 */
	public function registerPostTypes() {

		foreach ( $this->arr_cpts as $str_pt => $arr_args ) {

			// Ref: https:// developer.wordpress.org/reference/functions/register_post_type/ .
			$this->arr_ret[ $str_pt ] = register_post_type( $str_pt, $arr_args );
		}
	}

	/**
	 * Returns the array of results from the registerPostTypes()'s register_post_type().
	 *
	 * @return array
	 */
	public function getReturn() {

		return $this->arr_ret;
	}
}
